/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPTSProgressObserver.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkPTSProgressObserver
 * @brief
 *
 *
 */

#ifndef vtkPTSProgressObserver_h
#define vtkPTSProgressObserver_h

#include "vtkObject.h"
#include "vtkRemotingMicroservicesModule.h" // for exports

#include "vtkPythonObservableWrapper.h" // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE

#include <memory> // for std::unique_ptr

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;

class VTKREMOTINGMICROSERVICES_EXPORT vtkPTSProgressObserver : public vtkObject
{
public:
  static vtkPTSProgressObserver* New();
  vtkTypeMacro(vtkPTSProgressObserver, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // Get vtkProgressItem as a serialized json stream
  rxcpp::observable<std::string> GetServiceProgressObservable(const std::string& serviceName) const;
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    std::string, GetServiceProgressObservable(const std::string& serviceName) const);

  /**
   * Get a service's metrics/stats as a json formatted string.
   */
  rxcpp::observable<std::string> GetServiceMetricsObservable(const std::string& serviceName) const;
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    std::string, GetServiceMetricsObservable(const std::string& serviceName) const);

  ///@{
  /**
   * Get/set the session. Changing the session will cause the current/selection to
   * change and hence trigger `on_next` call on the subscribed observables, if any.
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

  void Unsubscribe();

protected:
  vtkPTSProgressObserver();
  ~vtkPTSProgressObserver() override;

private:
  vtkPTSProgressObserver(const vtkPTSProgressObserver&) = delete;
  void operator=(const vtkPTSProgressObserver&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
