import paraview.modules.vtkRemotingMicroservices
import paraview.modules.vtkRemotingPythonAsyncCore as asyncCore


class ProgressObserver(
    paraview.modules.vtkRemotingMicroservices.vtkPTSProgressObserver
):
    def __init__(self, session):
        self.SetSession(session)

    def GetServiceProgressObservable(self, service_name):
        return asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetServiceProgressObservable(service_name)
        )

    def GetServiceMetricsObservable(self, service_name):
        return asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetServiceMetricsObservable(service_name)
        )
