import asyncio

from parat.services import ParaT, PipelineBuilder, PropertyManager


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )

    pmanager = PropertyManager()
    pmanager.SetValues(sphere, Radius=5, force_push=True)
    pmanager.SetValues(sphere, Center=[1, 2, 3])
    pmanager.SetValues(sphere, Center=(3, 4, 5))
    pmanager.SetValues(sphere, **{"StartPhi": 10, "EndPhi": 90, "PhiResolution": 80})
    pmanager.Push(sphere)

    assert pmanager.GetValues(sphere, "Center") == {"Center": [3, 4, 5]}
    values = pmanager.GetValues(sphere)
    assert (
        values["StartPhi"] == 10
        and values["EndPhi"] == 90
        and values["PhiResolution"] == 80
    )

    values = pmanager.GetValues(shrink)
    assert values["Input"] == sphere and values["ShrinkFactor"] == 0.3

    await asyncio.sleep(0.1)

    status = await builder.DeleteProxy(sphere)
    assert status == False, "Cannot delete a proxy that has depedencies"

    status = await builder.DeleteProxy(shrink)
    assert status == True, "Could not delete shrink proxy"
    status = await builder.DeleteProxy(sphere)
    assert status == True, "Could not delete sphere source"

    await App.finalize()


asyncio.run(main())
