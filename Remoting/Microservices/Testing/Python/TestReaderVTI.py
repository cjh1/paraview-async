import asyncio

from parat.services import ParaT, PipelineBuilder, PropertyManager

from vtk.util.misc import vtkGetDataRoot
import os.path


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pm = PropertyManager()

    dataToLoad = os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti")

    reader = await builder.CreateProxy(
        "sources", "XMLImageDataReader", FileName=dataToLoad
    )

    await pm.UpdatePipeline(reader)

    info = reader.GetDataInformation()
    # print(info)
    assert info.GetNumberOfPoints() == 17139400
    assert info.GetNumberOfCells() == 16917660

    await App.finalize()


asyncio.run(main())
