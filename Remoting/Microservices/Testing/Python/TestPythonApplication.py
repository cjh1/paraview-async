import asyncio

from parat.services import ParaT

""" This example demostrates how to initialize and finalize an application in
the async framework. """


async def main():
    app = ParaT()
    session = await app.initialize()
    print(session)
    await asyncio.sleep(0.5)

    await app.finalize()


asyncio.run(main())
