/*=========================================================================

  Program:   ParaView
  Module:    vtkPythonAsyncCoreUtilities.txx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkPythonAsyncCoreUtilities_txx
#define vtkPythonAsyncCoreUtilities_txx

/* A set of utilities for setting a value of a asyncio.future from C++ */

#include "vtkPython.h" // include this first

#include "vtkPythonUtil.h"
#include "vtkSmartPointer.h"

#include <typeinfo> // operator typeid

// Check if any errors occurred on the python interpreter.
// If there is print the erro message and return true.
inline bool CheckAndClearError()
{
  if (PyObject* exception = PyErr_Occurred())
  {
    PyErr_Print();
    PyErr_Clear();
    return true;
  }
  return false;
}

inline void CheckReferenceCount(PyObject* object)
{
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (Py_REFCNT(object) == 0)
  {
    throw std::runtime_error("Reference count of PyObject is zero !");
  }
}

//----------------------------------------------------------------------------
// Create an exception of type `RuntimeError` and contents equal to `message`
// and set it as exception for `future` .
inline void SetFutureException(PyObject* future, const std::string& message)
{
  vtkPythonScopeGilEnsurer gilEnsurer;
#if PY_VERSION_HEX >= 0x3090000
  PyObject* pyMessage = PyString_FromString(message.c_str());

  PyObject* pyException = PyObject_CallOneArg(PyExc_RuntimeError, pyMessage);
#else
  PyObject* pyException = nullptr;
  PyObject* args = Py_BuildValue("(z)", message.c_str());
  if (args != nullptr)
  {
    pyException = PyObject_CallObject(PyExc_RuntimeError, args);
    Py_DECREF(args);
  }
#endif
  if (pyException == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("Error creating exception instance");
  }

  if (PyObject_CallMethod(future, "set_exception", "O", pyException) == nullptr)
  {
    CheckAndClearError();
    const std::string errorMessage = std::string("error setting future's exception to :") + message;
    throw std::runtime_error(errorMessage.c_str());
  }
}

//----------------------------------------------------------------------------
template <typename T>
void SetFutureValue(PyObject* future, T value)
{
  CheckReferenceCount(future);
  const std::string message = std::string("Undefined template specialization for this type") +
    std::string(typeid(value).name());
  SetFutureException(future, message);
}

//============================================================================
// Template specializations
//----------------------------------------------------------------------------
template <>
inline void SetFutureValue(PyObject* future, bool value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(future, "set_result", "O", PyBool_FromLong(value)) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
}

//----------------------------------------------------------------------------
template <>
inline void SetFutureValue(PyObject* future, long value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(future, "set_result", "l", value) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
}

//----------------------------------------------------------------------------
template <>
inline void SetFutureValue(PyObject* future, vtkTypeUInt32 value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(future, "set_result", "I", value) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
}

//----------------------------------------------------------------------------
template <>
inline void SetFutureValue(PyObject* future, const std::string& value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(future, "set_result", "s", value.c_str()) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
}

//----------------------------------------------------------------------------
template <typename T>
inline void SetFutureValue(PyObject* future, vtkSmartPointer<T> value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* pyValue = vtkPythonUtil::GetObjectFromPointer(value);
  if (PyObject_CallMethod(future, "set_result", "O", pyValue) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
  Py_DECREF(pyValue);
}

//----------------------------------------------------------------------------
template <typename T,
  typename SFINAE = typename std::enable_if<std::is_base_of<vtkObject, T>::value>::type>
inline void SetFutureValue(PyObject* future, T* value)
{
  CheckReferenceCount(future);
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* pyValue = vtkPythonUtil::GetObjectFromPointer(value);
  if (PyObject_CallMethod(future, "set_result", "O", pyValue) == nullptr)
  {
    SetFutureException(future, "Error setting future's value");
  }
  // pyValue is passed to the future so we can erase it now.
  Py_DECREF(pyValue);
  //  vtkPythonUtil::GetObjectFromPointer transfers the ownership to pyValue so we can reduce
  //  reference counter
  if (value)
  {
    value->Delete();
  }
}

//============================================================================
// Push an element to a asyncio.Queue
//----------------------------------------------------------------------------
template <typename T>
void PushToQueue(PyObject* queue, T value)
{
  (void)queue;
  (void)value;
  const std::string message =
    std::string("Undefined template specialization of PushToQueue for this type ") +
    std::string(typeid(value).name());
  throw std::runtime_error(message.c_str());
}

//----------------------------------------------------------------------------
template <>
inline void PushToQueue(PyObject* queue, int value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(queue, "put_nowait", "i", value) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
}

//----------------------------------------------------------------------------
template <>
inline void PushToQueue(PyObject* queue, long value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(queue, "put_nowait", "l", value) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
}

//----------------------------------------------------------------------------
template <>
inline void PushToQueue(PyObject* queue, std::string value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  if (PyObject_CallMethod(queue, "put_nowait", "s", value.c_str()) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
}

//----------------------------------------------------------------------------
template <typename T>
void PushToQueue(PyObject* queue, vtkSmartPointer<T> value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* pyValue = vtkPythonUtil::GetObjectFromPointer(value);
  if (PyObject_CallMethod(queue, "put_nowait", "O", pyValue) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
  // pyValue is passed to the queue so we can erase it now.
  Py_DECREF(pyValue);
}

//----------------------------------------------------------------------------
template <typename T>
void PushToQueue(PyObject* queue, const std::vector<vtkSmartPointer<T>>& value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* pyList = PyList_New(value.size());

  if (pyList == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("Error creating python list");
  }

  for (int i = 0; i < value.size(); i++)
  {
    PyObject* pyValue = vtkPythonUtil::GetObjectFromPointer(value[i]);
    if (PyList_SetItem(pyList, i, pyValue))
    {
      CheckAndClearError();
      throw std::runtime_error("error setting item from std::vector to python list");
    }
  }

  if (PyObject_CallMethod(queue, "put_nowait", "O", pyList) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
}

//----------------------------------------------------------------------------
template <>
void PushToQueue(PyObject* queue, std::tuple<vtkObject*, unsigned long, void*> value)
{
  CheckReferenceCount(queue);
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* pyValue;

  PyObject* pyObject = nullptr;
  if (std::get<0>(value))
  {
    pyObject = vtkPythonUtil::GetObjectFromPointer(std::get<0>(value));
  }
  else
  {
    pyObject = Py_None;
    Py_INCREF(Py_None);
  }
  // ignore last argument for now
  Py_INCREF(Py_None);
  pyValue = Py_BuildValue("[NkN]", pyObject, std::get<1>(value), Py_None);

  if (pyValue == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("Error creating python list");
  }

  if (PyObject_CallMethod(queue, "put_nowait", "O", pyValue) == nullptr)
  {
    CheckAndClearError();
    throw std::runtime_error("error pushing to iterator's queue");
  }
}
#endif
