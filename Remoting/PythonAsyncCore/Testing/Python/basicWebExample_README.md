# Build Instructions for web example:

```
python3 -m venv ./venv
source .venv/bin/activate
pip install -U pip
pip install trame
export PYTHONPATH=<buildDirectory>/lib/python<version>/site-packages

python basicWebExample.py
```
