import paraview.modules.vtkRemotingPythonAsyncCore as asyncCore
import paraview.modules.vtkRemotingServerManagerPython
import asyncio
from timeit import default_timer as timer


async def main():
    app = (
        paraview.modules.vtkRemotingServerManagerPython.vtkPVPythonApplication.GetInstance()
    )
    assert app

    loop = asyncCore.vtkPythonRunLoop.GetInstance()
    assert loop

    assert loop.IsAsyncInitialized() == True
    assert loop.HasRunningLoop() == False
    loop.AcquireRunningLoopFromPython()
    assert loop.HasRunningLoop() == True
    f = asyncCore.vtkPythonObservableWrapperUtilities.GetFuture(
        app.CreateBuiltinSession()
    )
    await f
    assert f.done() == True
    assert f.result() == 1

    Object = asyncCore.vtkObservableExamplePython()

    iterator = asyncCore.vtkPythonObservableWrapperUtilities.GetIterator(
        Object.GetObservable()
    )

    start = timer()
    async for x in iterator:
        now = timer()
        print(f"{x} {now-start:.3f}ms")
        start = now


asyncio.run(main())
