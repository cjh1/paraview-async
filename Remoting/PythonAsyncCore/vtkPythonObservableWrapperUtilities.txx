/*=========================================================================

  Program:   ParaView
  Module:    vtkPythonObservableWrapperUtilities.txx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkPythonObservableWrapperUtilities_txx
#define vtkPythonObservableWrapperUtilities_txx
#include "vtkPythonObservableWrapperUtilities.h"

#include "vtkLogger.h"
#include "vtkPythonAsyncCoreUtilities.txx"
#include "vtkPythonRunLoop.h"
#include "vtkSmartPyObject.h"

template <typename T>
PyObject* vtkPythonObservableWrapperUtilities::GetFutureInternal(
  vtkPythonObservableWrapper<T> wrapper)
{
  vtkPythonScopeGilEnsurer gilEnsurer;
  PyObject* future = vtkPythonRunLoop::GetInstance()->CreateFuture();
  rxcpp::observable<T> observable = wrapper.GetObservable();
  // Keep the future alive until `on_completed` is called regardless of what happens
  // on the Python side.
  Py_INCREF(future);
  observable.subscribe(
    // on_next
    [future](T value) {
      try
      {
        SetFutureValue(future, value);
      }
      catch (const std::exception& e)
      {
        vtkLogF(ERROR, "%s", e.what());
        throw e;
      }
    },
    // on_error
    [future](std::exception_ptr eptr) {
      try
      {
        std::rethrow_exception(eptr);
      }
      catch (const std::exception& e)
      {
        SetFutureException(future, e.what());
      }
    },
    // on_completed
    [future]() { Py_DECREF(future); });

  return future;
}

//----------------------------------------------------------------------------
template <typename T>
PyObject* vtkPythonObservableWrapperUtilities::GetIteratorInternal(
  vtkPythonObservableWrapper<T> wrapper)
{
  PyObject* iterator = vtkPythonRunLoop::GetInstance()->CreateAsyncIterator();
  rxcpp::observable<T> observable = wrapper.GetObservable();

  // Increase the reference count in case the iterator is discarded in the python side before the
  // end of the iterable
  Py_INCREF(iterator);
  observable.subscribe(

    // on_next
    [iterator](T value) {
      if (!vtkPythonRunLoop::GetInstance() || !Py_IsInitialized())
      {
        throw std::runtime_error("Python asyncio loop or interpreter are not available");
      }
      vtkPythonScopeGilEnsurer gilEnsurer;
      PyObject* queue = PyObject_CallMethod(iterator, "get_queue", "");
      if (queue == nullptr)
      {
        CheckAndClearError();
        throw std::runtime_error("error getting iterator's queue");
      }
      PushToQueue(queue, value);
    },

    // on_error
    [iterator](std::exception_ptr eptr) {
      if (!vtkPythonRunLoop::GetInstance() || !Py_IsInitialized())
      {
        throw std::runtime_error("Python asyncio loop or interpreter are not available");
      }
      vtkPythonScopeGilEnsurer gilEnsurer;
      PyObject* future = PyObject_CallMethod(iterator, "get_future", "");
      if (future == nullptr)
      {
        CheckAndClearError();
        throw std::runtime_error("error getting iterator's future");
      }
      try
      {
        std::rethrow_exception(eptr);
      }
      catch (const std::exception& e)
      {
        SetFutureException(future, e.what());
      }
    },

    // on_completed
    [iterator]() {
      // if `on_completed` is tied to an object that outlives the python
      // interpreter this function will be called at the very end.  We don't
      // consider this an error but we also avoid touching any Python related
      // code
      if (!vtkPythonRunLoop::GetInstance() || !Py_IsInitialized())
      {
        return;
      }
      vtkPythonScopeGilEnsurer gilEnsurer;
      PyObject* future = PyObject_CallMethod(iterator, "get_future", "");
      if (future == nullptr)
      {
        CheckAndClearError();
        throw std::runtime_error("error getting iterator's future");
      }

      SetFutureValue(future, true);
      Py_DECREF(iterator);
    });
  return iterator;
}

#endif
