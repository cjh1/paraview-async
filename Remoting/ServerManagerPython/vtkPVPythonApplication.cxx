/*=========================================================================

  Program:   ParaView
  Module:    vtkPVPythonApplication.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPVPythonApplication.h"

#include "vtkDistributedEnvironment.h"
#include "vtkLogger.h"
#include "vtkObjectFactory.h"
#include "vtkPythonRunLoop.h"

class vtkPVPythonApplication::vtkInternals
{
public:
  std::unique_ptr<vtkDistributedEnvironment> Environment;
};

vtkStandardNewMacro(vtkPVPythonApplication);
//----------------------------------------------------------------------------
vtkPVPythonApplication::vtkPVPythonApplication()
  : Internals(new vtkPVPythonApplication::vtkInternals())
{
}

//----------------------------------------------------------------------------
vtkPVPythonApplication::~vtkPVPythonApplication() = default;

//----------------------------------------------------------------------------
vtkPVPythonApplication* vtkPVPythonApplication::GetInstance()
{
  return vtkPVPythonApplication::SafeDownCast(vtkPVCoreApplication::GetInstance());
}
//----------------------------------------------------------------------------
bool vtkPVPythonApplication::InitializeUsingPython(
  const std::string& executable, vtkMultiProcessController* globalController)
{
  auto* runLoop = vtkPythonRunLoop::GetInstance();
  if (runLoop == nullptr)
  {
    vtkLogF(ERROR, "Initialize vtkPythonRunLoop first !");
    return false;
  }

  if (globalController == nullptr)
  {
    this->Internals->Environment = std::make_unique<vtkDistributedEnvironment>(nullptr, nullptr);
    globalController = this->Internals->Environment->GetController();
  }

  return this->Initialize(executable, runLoop->observe_on_run_loop(), globalController);
}

//----------------------------------------------------------------------------
void vtkPVPythonApplication::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
