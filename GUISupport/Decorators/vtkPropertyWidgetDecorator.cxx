/*=========================================================================

   Program: ParaView
   Module:    $RCSfile$

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#include "vtkPropertyWidgetDecorator.h"
#include "vtkCompositePropertyWidgetDecorator.h"
#include "vtkEnableWidgetDecorator.h"
#include "vtkGenericPropertyWidgetDecorator.h"
#include "vtkInputDataTypeDecorator.h"
#include "vtkMultiComponentsDecorator.h"
#include "vtkOSPRayHidingDecorator.h"
#include "vtkObjectFactory.h"
#include "vtkPVLogger.h"
#include "vtkPVXMLElement.h"
#include "vtkShowWidgetDecorator.h"

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkPropertyWidgetDecorator);

//-----------------------------------------------------------------------------
vtkPropertyWidgetDecorator::vtkPropertyWidgetDecorator() = default;

//-----------------------------------------------------------------------------
vtkPropertyWidgetDecorator::~vtkPropertyWidgetDecorator() = default;

//-----------------------------------------------------------------------------
void vtkPropertyWidgetDecorator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//-----------------------------------------------------------------------------
void vtkPropertyWidgetDecorator::initialize(vtkPVXMLElement* config, vtkSMProxy* proxy)
{
  this->XML = config;
  this->Proxy = proxy;
}

//-----------------------------------------------------------------------------
vtkSMProxy* vtkPropertyWidgetDecorator::proxy() const
{
  return this->Proxy;
}

//-----------------------------------------------------------------------------
vtkPVXMLElement* vtkPropertyWidgetDecorator::xml() const
{
  return this->XML.GetPointer();
}
//-----------------------------------------------------------------------------
void vtkPropertyWidgetDecorator::InvokeVisibilityChangedEvent()
{
  this->InvokeEvent(VisibilityChangedEvent);
}

//-----------------------------------------------------------------------------
void vtkPropertyWidgetDecorator::InvokeEnableStateChangedEvent()
{
  this->InvokeEvent(EnableStateChangedEvent);
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPropertyWidgetDecorator> vtkPropertyWidgetDecorator::create(
  vtkPVXMLElement* xmlconfig, vtkSMProxy* proxy)
{
  if (xmlconfig == nullptr || strcmp(xmlconfig->GetName(), "PropertyWidgetDecorator") != 0 ||
    xmlconfig->GetAttribute("type") == nullptr)
  {
    vtkLogF(WARNING, "Invalid xml config specified. Cannot create a decorator.");
    return nullptr;
  }

  vtkSmartPointer<vtkPropertyWidgetDecorator> decorator;
  const std::string type = xmlconfig->GetAttribute("type");
  // *** NOTE: When adding new types, please update the header documentation ***
  if (type == "CTHArraySelectionDecorator")
  {
    // return new pqCTHArraySelectionDecorator(config, widget);
  }
  if (type == "InputDataTypeDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkInputDataTypeDecorator::New());
  }
  if (type == "EnableWidgetDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkEnableWidgetDecorator::New());
  }
  if (type == "ShowWidgetDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkShowWidgetDecorator::New());
  }
  if (type == "GenericDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkGenericPropertyWidgetDecorator::New());
  }
  if (type == "OSPRayHidingDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkOSPRayHidingDecorator::New());
  }
  if (type == "MultiComponentsDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkMultiComponentsDecorator::New());
  }
  if (type == "CompositeDecorator")
  {
    decorator = vtk::TakeSmartPointer(vtkCompositePropertyWidgetDecorator::New());
  }
  if (type == "SessionTypeDecorator")
  {
    // return new pqSessionTypeDecorator(config, widget);
  }

  if (decorator)
  {
    decorator->initialize(xmlconfig, proxy);
  }

  else
  {
    vtkVLog(vtkLogger::VERBOSITY_WARNING, << "Cannot create decorator of type " << type);
  }

  // *** NOTE: When adding new types, please update the header documentation ***
  return decorator;
}
