# Build dev, runtime docker containers
```shell
$ ./build-all.sh
```

# Run it
Please click on the link with ip=`127.0.0.1`

```shell
$ ./run-docker.sh TestSCDemo.py 8080
2022-10-28 20:27:07.033 (   0.004s) [Parat           ]  vtkServicesEngine.cxx:145   INFO| vtkThalliumServicesEngine (0x55bd25e30390): initializing session (sockets://)

App running at:
 - Local:   http://127.0.0.1:8080/
 - Network: http://127.0.1.1:8080/

Note that for multi-users you need to use and configure a launcher.
```

# Remote rendering
These options are available for image delivery: `VP9`, `VP9_WEBM`, `JPG`, `RGBA` and `RGB`.
At this point, if you select `H264_NVENC`, the server simply reports an error that it could not find NVIDIA drivers.
The server does not exit or crash. You can fall back to other options.
