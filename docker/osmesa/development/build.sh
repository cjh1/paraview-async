#!/usr/bin/env bash
SCRIPT_DIR=`dirname "$0"`
ROOT_DIR=$SCRIPT_DIR/../../
docker build --rm -t parat-osmesa-dev -f $SCRIPT_DIR/Dockerfile $ROOT_DIR
