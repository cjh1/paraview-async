#!/usr/bin/env bash
#
# ./run-docker.sh <Prototype name> <Port number>
# Ex: ./run-docker.sh TestSCDemo2.py 8080
#

docker run \
  -it \
  --network="host" \
  -e PARAT_PROTOTYPE=$1 \
  -e PARAT_PORT=$2 \
  parat-osmesa-run:latest
