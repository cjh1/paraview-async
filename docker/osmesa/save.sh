#!/usr/bin/env bash
DATE=`date "+%Y_%m_%d"`
docker save parat-osmesa-run | gzip > "parat-osmesa-run-$DATE.tgz"
