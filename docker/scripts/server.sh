#!/bin/sh

export LD_LIBRARY_PATH="/usr/lib/x86_64-linux-gnu:${PARAT_LD_PATH}"
export PYTHONPATH="${PARAT_RUNTIME_DIR}/lib/python3.8/site-packages"
export PATH="${PARAT_RUNTIME_DIR}/bin:${PATH}"

pvpython ${PARAT_WEB_PROTOTYPE_DIR}/${PARAT_PROTOTYPE} -D ${PARAT_DATA_DIR} --server --host 127.0.0.1 --port ${PARAT_PORT}
