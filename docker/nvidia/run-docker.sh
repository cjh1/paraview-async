#!/usr/bin/env bash
#
# ./run-docker.sh <Prototype name> <Port number>
# Ex: ./run-docker.sh TestSCDemo2.py 8080
#

docker run \
  -it \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  --gpus all \
  --network="host" \
  -e NVIDIA_DRIVER_CAPABILITIES=all \
  -e PARAT_PROTOTYPE=$1 \
  -e PARAT_PORT=$2 \
  parat-run:latest
