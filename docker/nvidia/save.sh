#!/usr/bin/env bash
DATE=`date "+%Y_%m_%d"`
docker save parat-run | gzip > "parat-run-$DATE.tgz"
