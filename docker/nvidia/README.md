# Setup nvidia-docker2
The process is explained step-by-step [here](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html). Here's an outline.

1. You will have to install `nvidia-docker2` and restart docker daemon.
2. Please run the test `docker run --rm --gpus all nvidia/cuda:11.0.3-base-ubuntu20.04 nvidia-smi` to verify `nvidia-smi` works inside a container before proceeding.

# Build dev, runtime docker containers
```shell
$ ./build-all.sh
```

# Run it
Please click on the link with ip=`127.0.0.1`

```shell
$ ./run-docker.sh TestSCDemo.py 8080
2022-10-28 20:27:07.033 (   0.004s) [Parat           ]  vtkServicesEngine.cxx:145   INFO| vtkThalliumServicesEngine (0x55bd25e30390): initializing session (sockets://)

App running at:
 - Local:   http://127.0.0.1:8080/
 - Network: http://127.0.1.1:8080/

Note that for multi-users you need to use and configure a launcher.
```

# Remote rendering
These options are available for image delivery: `VP9`, `VP9_WEBM`, `H264_NVENC`, `JPG`, `RGBA` and `RGB`.
