set(test_exclusions)

list(APPEND test_exclusions
  # Nothing to test 
  "^ParaView::pqCoreCxx-TestBasicQtApp$"
  # qt tests
  "^pqWidgetsAnimation$"
  "^pqWidgetsFlatTreeView$"
  "^pqWidgetsHeaderViewCheckState$"
  "^pqWidgetsTreeViewSelectionAndCheckState$"
  "^pqWidgetsDoubleLineEdit$"
  "^pqWidgetsHierarchicalGridLayout$"
  "^pqWidgetspqTextEditTest$"
  # currently broken on the ci
  "^pv.TestDevelopmentInstall$"
  )

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
