#!/bin/sh

set -e

# Install build requirements.
dnf install -y --setopt=install_weak_deps=False \
     mesa-dri-drivers mesa-libGL-devel \
     python-devel

# Qt dependencies
dnf install -y --setopt=install_weak_deps=False \
    qt5-qtbase-devel qt5-qttools-devel qt5-qtsvg-devel \
    qt5-qtxmlpatterns-devel libXcursor-devel

# Development tools
dnf install -y --setopt=install_weak_deps=False \
    gcc gcc-c++ gcc-gfortran \
    git-core

# Install development tools for thallium to reduce build time
dnf install -y --setopt=install_weak_deps=False \
    autoconf automake \
    boost-devel \
    cereal-devel \
    json-c-devel \
    libtool libsigsegv-devel \
    make m4 \
    patch patchelf perl pkgconf \
    xz lbzip2

dnf clean all
