#!/bin/sh

set -e

readonly version="2.15.05"

case "$( uname -s )" in
    Linux)
        shatool="sha256sum"
        sha256sum="a51d1c3dae9265e3c714bd285aac303733e98df867aa3028b624a0d0d18809e4"
        platform="linux"
        filename="nasm-$version-0.fc31.x86_64.rpm"
        ;;
    Darwin)
        shatool="shasum -a 256"
        sha256sum="6c1368ee6252b9ba7825011e5e2446fa8d6b713e7d87f19fe1933682e32c6d90"
        platform="macosx"
        filename="nasm-$version-macosx.zip"
        ;;
    *)
        echo "Unrecognized platform $( uname -s )"
        exit 1
        ;;
esac
readonly shatool
readonly sha256sum
readonly platform
readonly filename

cd .gitlab
echo "$sha256sum  $filename" > nasm.sha256sum
curl -OL "https://www.nasm.us/pub/nasm/releasebuilds/$version/$platform/$filename"
$shatool --check nasm.sha256sum

case "$( uname -s )" in
    Linux)
        mkdir -p nasm-work
        mv $filename nasm-work/
        cd nasm-work
        rpm2archive $filename
        ../cmake/bin/cmake -E tar xf "$filename.tgz"
        cd ..
        mv nasm-work/usr/bin/nasm .
        ;;
    Darwin)
        tarball="$filename"
        ./cmake/bin/cmake -E tar xf "$tarball"
        mv "nasm-$version/nasm" .
        ;;
    *)
        echo "Unrecognized platform $( uname -s )"
        exit 1
        ;;
esac
