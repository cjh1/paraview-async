# Build Intructions for ParaView Async

## Initialize repository
```
git clone git@gitlab.kitware.com:async/paraview.git
cd paraview
git submodule update --init --recursive
cd -
```

## Setup mochi-thallium

### Setup spack
```
git clone https://github.com/spack/spack.git
export SPACK_ROOT=$PWD/spack
. ./spack/share/spack/setup-env.sh
```

### Mochi-thallium
```
git clone https://github.com/mochi-hpc/mochi-spack-packages.git
spack repo add mochi-spack-packages
spack install mochi-thallium@0.10.1 ^libfabric fabrics=tcp,rxm,sockets ^mochi-margo@0.9.10
```
If there are issues with `libfabric` on other platforms try with `fabrics=sockets`.

## Image delivery

### Software based:
`nasm` and `perl` are needed to build the `libvpx` codec sdk for VP9 encoder and decoder.
You may download `nasm` from https://www.nasm.us/ or your distribution package manager. `perl`
is usually installed by default.

### Hardware Accelerated (nvidia):
- Build dependency: none
- Runtime dependency: needs an NVIDIA GPU with NVENC capabilities and a working nvidia driver.
Please visit https://developer.nvidia.com/video-encode-and-decode-gpu-support-matrix-new to check
if your GPU is supported.

## Build
```
export SPACK_ROOT=$PWD/spack
. ./spack/share/spack/setup-env.sh
spack load mochi-thallium

cmake -G Ninja \
    -S ./paraview \
    -B ./build \
    -D PARAVIEW_BUILD_TESTING=WANT \
    -D PARAVIEW_USE_PYTHON=ON
```

## Extras

### Python
For python support add `-DPARAVIEW_USE_PYTHON=ON`. Set environment variable to ensure its picked up by `python`.
```bash
export PYTHONPATH=./build/lib/pythonx.y/site-packages
```

### Ospray
For raytracing [get](https://github.com/ospray/ospray/releases/tag/v2.9.0) ospray binaries and set
`-DPARAVIEW_ENABLE_RAYTRACING=ON` and  `-Dospray_DIR` to the appropriate location. For Linux  it
is  `-Dospray_DIR="<extracted location>/ospray-2.9.0.x86_64.linux/lib/cmake/ospray-2.9.0"`.
When running any application utilizing raytracing you may need to update the library path. For Linux:
`export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<extracted location>/ospray-2.9.0.x86_64.linux/lib/`

### Docker

Refer to [docker/nvidia](docker/nvidia/README.md) to get the prototype up and running on
nvidia GPU and [docker/osmesa](docker/osmesa/README.md) for OSMesa if the machine lacks
nvidia drivers or nvidia hardware.
